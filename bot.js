'use strict';

const fs = require('fs');
const rp = require('request-promise-native');
const format = require('string-format');
const config = require('./config.json');

format.extend(String.prototype);

const checkGames = () => {
	const options = {
		json: true,
		qs: {
			filter: 'all',
			page_size: config.humblebundle.game_count,
			request: 1,
			sort: 'discount'
		},
		uri: 'https://www.humblebundle.com/store/api/search'
	};

	rp(options)
		.then(games => {
			games.results.filter(game => game.current_price[0] === 0).forEach(game => {
				fs.readFile(config.savefile, (save_err, data) => {
					const save = save_err ? {} : JSON.parse(data);
					const friendly_name = game.human_name.replace(/[^a-zA-Z ]/g, '');
					if (!(friendly_name in save) || save[friendly_name] !== game.sale_end) {
						game.link = `https://www.humblebundle.com/store/${game.human_url}`;
						game.expires = new Date(game.sale_end * 1000).toUTCString();
						save[friendly_name] = game.sale_end;
						fs.writeFile(config.savefile, JSON.stringify(save), (err) => {
							if (err) {
								console.log('Failed to save game. Not sending to prevent spam.');
							} else if (config.telegram.active) {
								rp({
									qs: {
										chat_id: config.telegram.chat_id,
										disable_notification: config.message.disable_notification,
										disable_web_page_preview: config.message.disable_web_page_preview,
										parse_mode: config.message.parse_mode,
										reply_markup: config.message.button ? Object.keys(config.message.custom_reply_markup).length
											? JSON.stringify(config.message.custom_reply_markup)
											: JSON.stringify({ inline_keyboard: [ [ { text: `Get ${game.human_name} Now!`, url: game.link } ] ] })
											: {},
										text: config.message.text.format(game)
									},
									uri: 'https://api.telegram.org/bot' + config.telegram.token + '/sendMessage'
								})
									.then(() => {
										console.log(`Sent game: ${game.human_name}`);
									})
									.catch(error => {
										console.log(`An error occured whilst sending the game to Telegram: ${error}`);
									});
							} else {
								console.log(`Game: ${game.human_name}\nFriendly name: ${friendly_name}\nSale ends: ${game.sale_end}`);
							}
						});
					}
				});
			});
		})
		.catch(e => console.log(e));
};

checkGames();
setInterval(() => {
	checkGames();
}, config.check_every_minutes * 60000);
